import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, Tab, Tabs } from 'ionic-angular';

import { App } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { ForgotPage } from '../pages/forgot/forgot';
import { ForgotSetPage } from '../pages/forgotset/forgot.set';
import { RegisterStartPage } from '../pages/register-start/register-start';
import { HttpClientModule } from '@angular/common/http';
import { DevicesPage } from '../pages/devices/devices';
import { EditDevicePage } from '../pages/devices/device.edit';
import { NewDevicePage } from '../pages/devices/device.new';
import { NewScheduleDevicePage } from '../pages/devices/device.schedule.new';
import { SchedulesDevicePage } from '../pages/devices/device.schedules';
import { DeviceView } from '../pages/devices/device.view';
import { DeviceTabs } from '../pages/devices/device.tabs';
import { GenerateApiKey } from '../pages/profile/profile.genkey';
import { GetApiSecretKey } from '../pages/profile/profile.getkey';
import { APIs } from '../pages/apis/apis';
import { Developer } from '../pages/apis/developer.tab';
import { MQTT } from '../pages/apis/mqtt';
import { NewAPIKey } from '../pages/apis/apis.new';
import { APIKeyView } from '../pages/apis/apis.view';

@NgModule({
  declarations: [
    App,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    ForgotPage,
    ForgotSetPage,
    RegisterStartPage,
    DevicesPage,
    EditDevicePage,
    NewDevicePage,
    NewScheduleDevicePage,
    SchedulesDevicePage,
    DeviceTabs,
    DeviceView,
    GenerateApiKey,
    GetApiSecretKey,
    APIs,
    NewAPIKey,
    APIKeyView,
    Developer,
    MQTT
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(App),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    App,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    ForgotPage,
    ForgotSetPage,
    RegisterStartPage,
    DevicesPage,
    EditDevicePage,
    NewDevicePage,
    NewScheduleDevicePage,
    SchedulesDevicePage,
    DeviceTabs,
    DeviceView,
    GenerateApiKey,
    GetApiSecretKey,
    APIs,
    NewAPIKey,
    APIKeyView,
    Developer,
    MQTT
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
