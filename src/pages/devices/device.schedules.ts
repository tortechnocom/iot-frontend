import { Component } from '@angular/core';
import 'rxjs/Rx';
import {NavController, AlertController, NavParams, ToastController, ModalController} from 'ionic-angular';
import {App} from '../../app/app.component';
import { NewScheduleDevicePage } from './device.schedule.new';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'device-schedules-page',
  templateUrl: 'device.schedules.html'
})
export class SchedulesDevicePage {
  private deviceScheduleList: any;
  private deviceId: any;
  constructor(public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
  ) {
    this.deviceId = navParams.data;
    this.getDeviceScheduleList();
  }
  getDeviceScheduleList(){
    console.log("TimeZone: " + Intl.DateTimeFormat().resolvedOptions().timeZone)
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotGetDeviceScheduleList', {deviceId: this.deviceId}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceScheduleList = res['deviceScheduleList'];
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  newDeviceSchedule(deviceId) {
    var callback = () => {
      return new Promise((resolve, reject) => {
        this.getDeviceScheduleList();
         resolve();
      });
    }
    let modal = this.modalCtrl.create(NewScheduleDevicePage, {callback: callback, deviceId: deviceId});
    modal.present();
  }
  disableDeviceSchedule(jobId) {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotDisableDeviceSchedule', {jobId: jobId}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          let toast = this.toastCtrl.create({
            message: res['successMessage'],
            duration: 1000,
            position: 'bottom'
          });
          toast.present();
          this.getDeviceScheduleList();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  enableDeviceSchedule(jobId) {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotEnableDeviceSchedule', {jobId: jobId}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          let toast = this.toastCtrl.create({
            message: res['successMessage'],
            duration: 1000,
            position: 'bottom'
          });
          toast.present();
          this.getDeviceScheduleList();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  deleteDeviceSchedule(jobId) {
    let alertConfirm = this.alertCtrl.create({
      title: '',
      message: 'Do you want to remove schedule?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            let headerJson = {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': App.authToken
              };
              this.httpClient.post(App.apiUrl + 'iotDeleteDeviceSchedule', {jobId: jobId}, {
                headers: new HttpHeaders(headerJson)
              })
              .subscribe(res => {
                let title = "Response";
                if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
                  let alert = this.alertCtrl.create({
                    title: title,
                    subTitle: res['errorMessage'],
                    buttons: ['OK']
                  });
                  alert.present();
                } else {
                  this.getDeviceScheduleList();
                }
                //resolve(res);
              }, (err) => {
                console.log('err: ', err);
                //reject(err);
              });
          }
        }
      ]
    });
    alertConfirm.present();
  }
  toLocaleString(date) {
    if (date ==  null) return; 
    let browserStartDate = new Date();
    let runTimeDate = new Date(date);
    let actualStartTime = runTimeDate.getTime() - (browserStartDate.getTimezoneOffset() * 60 * 1000);
    return new Date(actualStartTime);
  }
}