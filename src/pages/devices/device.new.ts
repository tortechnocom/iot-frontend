import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {NavController, AlertController} from 'ionic-angular';
import {App} from '../../app/app.component';
import {DevicesPage} from './devices';
import { EditDevicePage } from './device.edit';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceTabs } from './device.tabs';


@Component({
  selector: 'device-new-page',
  templateUrl: 'device.new.html'
})
export class NewDevicePage {
  private input: any = {
    deviceName: "",
    description: "",
    deviceTypeId: ""
  };
  private deviceTypeList: any;
  constructor(
    public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController
  ) {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotGetDeviceTypeList', this.input, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceTypeList = res['deviceTypeList'];
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  
  goBack() {
    this.navCtrl.pop();
  }
  addDevice() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotAddDevice', this.input, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          if (res['deviceId']) {
            this.navCtrl.push(DevicesPage,{
              deviceId: res['deviceId']
            });
          }
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  cancelDevice() {
    this.navCtrl.setRoot(DevicesPage);
  }
}