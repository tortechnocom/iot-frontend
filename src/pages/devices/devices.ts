import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { NewDevicePage } from './device.new';
import { EditDevicePage } from './device.edit';
import { SchedulesDevicePage } from './device.schedules';
import {Http} from '@angular/http';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceView } from './device.view';
import { DeviceTabs } from './device.tabs';

declare var Paho: any;
@Component({
  selector: 'devices-page',
  templateUrl: 'devices.html'
})
export class DevicesPage {
  private deviceList: any;
  public static deviceMap: any = {};
  private mqttClient: any;
  public options: any;
  private showList = false;
  private daskboardColor = "dark";
  private listColor = "disable";
  
  constructor(public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController,
    public nav: NavController,
    public loadingCtrl: LoadingController
    ) {
    App.Paho = Paho;
    localStorage.setItem("reconnect", "true");
    //mqtt
    this.options = {
      onSuccess: this.onConnect,
      userName: App.mqtt.username,
      password: App.mqtt.password,
      useSSL: (App.mqtt.secure == "true"),
      invocationContext: this,
      cleanSession: false
    }
    this.mqttClient = new App.Paho.MQTT.Client(
      App.mqtt.serverUrl,
      Number(App.mqtt.serverWebSocketPort),
      "iot-dashboard-" + App.user.partyId + "-" + new Date().getTime()
      );
      this.mqttClient.onConnectionLost = function(responseObject) {
          console.log("=====> Lost");
          this.connectOptions.invocationContext.reconnect();
        }
        this.mqttClient.onMessageArrived = function (message) {
        let payload = message._getPayloadString();
        let destinationName = message._getDestinationName();
        let destinationArray = destinationName.split("/");
        let deviceId = destinationArray[destinationArray.length-1];
        console.log("----------------------");
        console.log("topic: ", destinationName);
        console.log("payload: ", payload);
        for (let key in this.connectOptions.invocationContext.deviceList) {
          if (this.connectOptions.invocationContext.deviceList[key].deviceId == deviceId) {
            this.connectOptions.invocationContext.deviceList[key].payload = payload;
          }
        }
      };
    this.mqttClient.connect(this.options);
    
    //get device list
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotGetDeviceList', {}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceList = res['deviceList'];
          this.setDisplay(null);
          if (this.mqttClient.isConnected()) {
            this.subscribe(App.mqtt.topic);
          }
          loader.dismiss();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  newDevice() {
    this.navCtrl.push(NewDevicePage);
  }
  removeDevice(deviceId, deviceName) {
    let confirm = this.alertCtrl.create({
      title: 'Remove Device',
      message: 'Do you want to remove Device [' + deviceName +']?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Remove',
          handler: () => {
            let headerJson = {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': App.authToken
              };
              this.httpClient.post(App.apiUrl + 'iotRemoveDevice', {deviceId: deviceId}, {
                headers: new HttpHeaders(headerJson)
              })
              .subscribe(res => {
                let title = "Response";
                if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
                  let alert = this.alertCtrl.create({
                    title: title,
                    subTitle: res['errorMessage'],
                    buttons: ['OK']
                  });
                  alert.present();
                } else {
                    this.httpClient.post(App.apiUrl + 'iotGetDeviceList', {}, {
                      headers: new HttpHeaders(headerJson)
                    })
                    .subscribe(res => {
                      let title = "Response";
                      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
                        let alert = this.alertCtrl.create({
                          title: title,
                          subTitle: res['errorMessage'],
                          buttons: ['OK']
                        });
                        alert.present();
                      } else {
                        this.deviceList = res['deviceList'];
                      }
                      //resolve(res);
                    }, (err) => {
                      console.log('err: ', err);
                      //reject(err);
                    });
                }
                //resolve(res);
              }, (err) => {
                console.log('err: ', err);
                //reject(err);
              });
          }
        }
      ]
    });
    confirm.present();
  }
  editDevice(deviceId) {
    localStorage.setItem('reconnect', 'false');
    this.navCtrl.push(EditDevicePage, {deviceId: deviceId});
  }
  schedules(deviceId) {
    localStorage.setItem("reconnect", "false");
    this.navCtrl.push(SchedulesDevicePage, {deviceId: deviceId});
  }
  subscribe(topic) {
    var subscribeOptions = {
        qos: 0,  // QoS
        invocationContext: {foo: true},  // Passed to success / failure callback
        onSuccess: this.onSuccessCallback,
        onFailure: this.onFailureCallback,
        timeout: 100
    }
    this.mqttClient.subscribe(topic, subscribeOptions);
  }
  publish(device, event) {
    let text = "";
    if (event.checked) {
      text = "ON";
    } else {
      text = "OFF";
    }
    let message = new App.Paho.MQTT.Message(text);
    message.destinationName = device.mqtt.topic;
    message.qos = 0;
    
    // set device payload
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotSetDevicePayload',
      {
        deviceId: device.deviceId, payload: text}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.mqttClient.send(message);
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  onSuccessCallback () {
    //console.log("==> onSuccessCallback");
  }
  onFailureCallback () {
    //console.log("==> onFailureCallback");
  }
  onConnect() {
    console.log("==> onConnect");
    //console.log(this.mqttClient.isConnected());
  }
  reconnect() {
    this.mqttClient.connect(this.options);
  }
  goToDevice(deviceId) {
    var reloadDeviceList = () => {
      return new Promise((resolve, reject) => {
        this.getDeviceList();
         resolve();
      });
    }
    let device = null;
    for (let key in this.deviceList) {
      if (deviceId == this.deviceList[key].deviceId) {
        device = this.deviceList[key];
        break;
      }
    }
    this.nav.push(DeviceTabs, {deviceId: deviceId, callback: reloadDeviceList, device: device});
  }
  getDeviceList() {
    //get device list
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotGetDeviceList', {}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceList = res['deviceList'];
          if (this.mqttClient.isConnected()) {
            this.subscribe(App.mqtt.topic);
          }
          
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
  setDisplay(type) {
    if (type == null) {
      type = window.localStorage.getItem("DEVICE_VIEW");
    } else {
      window.localStorage.setItem("DEVICE_VIEW", type);
    }
    if (type == "list") {
      this.daskboardColor = "disable";
      this.listColor = "dark";
      this.showList = true;
    } else {
      this.daskboardColor = "dark";
      this.listColor = "disable";
      this.showList = false;
    }
  }
}



