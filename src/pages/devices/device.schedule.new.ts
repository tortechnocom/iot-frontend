import { Component } from '@angular/core';
import 'rxjs/Rx';
import {NavController, AlertController, NavParams, ToastController, ModalController, ViewController, DateTime} from 'ionic-angular';
import {App} from '../../app/app.component';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { DeviceTabs } from './device.tabs';
import moment from 'moment';


@Component({
  selector: 'device-schedule-new-page',
  templateUrl: 'device.schedule.new.html'
})
export class NewScheduleDevicePage {
  private deviceScheduleSetting: any;
  private device: any;
  private deviceId: any;
  private minDate: any;
  private maxDate: any;
  private callback: any;
  private input: any = {
    scheduleName: "",
    description: "",
    frequency: "04",
    typeId: "S_TYPE_REPEAT",
    deviceId: "",
    startDate: "",
    endDate: "",
    interval: 1,
    count: -1,
    actionId: "ON"
  };
  constructor(public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) {
    this.device = DeviceTabs.device;
    this.deviceId = navParams.data;
    this.callback = navParams.get("callback");

    let browserStartDate = new Date();
    let actualStartTime = browserStartDate.getTime() - (browserStartDate.getTimezoneOffset() * 60 * 1000);
    this.input.startDate = new Date(actualStartTime).toISOString();

    this.deviceScheduleSetting = {actionList: [], frequencyList: []};
    let now = new Date(actualStartTime);
    let oneYr = new Date(actualStartTime);
    oneYr.setFullYear(now.getFullYear() + 1);
    this.minDate = this.input.startDate
    this.maxDate = oneYr.toISOString();
    this.input.deviceId = navParams.get("deviceId");
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotGetDeviceScheduleSetting', {deviceId: this.deviceId}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceScheduleSetting = res['deviceScheduleSetting'];
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  
  cancel() {
    localStorage.setItem("reconnect", "true");
    this.viewCtrl.dismiss();
  }
  createDeviceSchedule(){
    let browserStartDate = new Date(this.input.startDate);
    let actualStartTime = browserStartDate.getTime() + (browserStartDate.getTimezoneOffset() * 60 * 1000);
    this.input.startDate = new Date(actualStartTime);
    if (this.input.typeId != "S_TYPE_REPEAT") {
      this.input.count = 1;
      this.input.frequency = "";
    }
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotCreateDeviceSchedule', this.input, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.viewCtrl.dismiss();
          this.callback();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
}