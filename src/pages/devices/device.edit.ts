import { Component } from '@angular/core';
import 'rxjs/Rx';
import {NavController, AlertController, NavParams, ToastController} from 'ionic-angular';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceTabs } from './device.tabs';


@Component({
  selector: 'device-edit-page',
  templateUrl: 'device.edit.html'
})
export class EditDevicePage {
  private input: any;
  private deviceTypeList: any;
  private deviceIconList: any;
  constructor(public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public navParams: NavParams) {
    this.input = DeviceTabs.device;
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotGetDeviceIconList', null, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceIconList = res['deviceIconList'];
          this.selectIcon(this.input.deviceIconId);
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    this.httpClient.post(App.apiUrl + 'iotGetDeviceTypeList', null, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.deviceTypeList = res['deviceTypeList'];
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });

  }
  
  goBack() {
    this.navCtrl.pop();
  }
  updateDevice() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
    this.httpClient.post(App.apiUrl + 'iotUpdateDevice', this.input, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        DeviceTabs.device = this.input;
        DeviceTabs.callback();
        let toast = this.toastCtrl.create({
          message: res['successMessage'],
          duration: 1000
        });
        toast.present();
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  selectIcon(iconId) {
    this.input.deviceIconId = iconId;
    let count = 0;
    for (let icon in this.deviceIconList) {
      if (iconId == this.deviceIconList[count].deviceIconId ) {
        this.deviceIconList[count].selected = "primary";
        this.input.deviceIconName = this.deviceIconList[count].name;
      } else {
        this.deviceIconList[count].selected = "";
      }
      count++;
    }
  }
  copyToClipboard(text) {
    var textArea = document.createElement("textarea");
  
    textArea.style.position = 'fixed';
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    textArea.style.padding = "0";
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();
    try {
      var successful = document.execCommand('copy');
      let toast = this.toastCtrl.create({
      message: 'Copy to clipboard succesfully',
      duration: 1000
      });
      toast.present();
    } catch (err) {
      console.log('Oops, unable to copy');
    }
    document.body.removeChild(textArea);
  }
}