import { Component, ViewChild } from '@angular/core';
import { App } from '../../app/app.component';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceTabs } from './device.tabs'
import { Chart } from 'chart.js';


@Component({
  selector: 'device-view',
  templateUrl: 'device.view.html'
})
export class DeviceView {
  device: any;
  mqttClient: any
  options: any;
  color: any;

  @ViewChild('lineCanvas') lineCanvas;
  times = [];
  values = [];
  lineChart: any;
  duration = "24h";
  
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public httpClient: HttpClient
    ) {
      this.device = DeviceTabs.device;
      //MQTT
      this.options = {
        onSuccess: this.onConnect,
        userName: App.mqtt.username,
        password: App.mqtt.password,
        useSSL: (App.mqtt.secure == "true"),
        invocationContext: this,
        cleanSession: false
      }
      this.mqttClient = new App.Paho.MQTT.Client(
        App.mqtt.serverUrl,
        Number(App.mqtt.serverWebSocketPort),
        "iot-dashboard-" + App.user.partyId + "-" + new Date().getTime()
      );
      this.mqttClient.onConnectionLost = function(responseObject) {
        console.log("==> Lost");
        this.connectOptions.invocationContext.reconnect();
      }
      this.mqttClient.onMessageArrived = function (message) {
        let payload = message._getPayloadString();
        this.connectOptions.invocationContext.device.payload = payload;
        if (payload == "ON") {
          this.connectOptions.invocationContext.color = "primary";
        } else {
          this.connectOptions.invocationContext.color = "";
        }
      };
      this.mqttClient.connect(this.options);
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
        this.httpClient.post(App.apiUrl + 'iotGetDevice', {deviceId: navParams.data}, {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              DeviceTabs.device = res['device'];
              this.device = DeviceTabs.device;
              if (this.device.deviceTypeId == "DEVICE-TEMPERATURE" || this.device.deviceTypeId == "DEVICE-HUMIDITY") {
                this.createChart();
              }
              
              if (this.device.payload == "ON") {
                this.color = "primary";
              }
              if (this.mqttClient.isConnected()) {
                this.subscribe();
              }
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
          
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
  subscribe() {
    var subscribeOptions = {
        qos: 0,  // QoS
        invocationContext: {foo: true},  // Passed to success / failure callback
        onSuccess: this.onSuccessCallback,
        onFailure: this.onFailureCallback,
        timeout: 300
    }
    this.mqttClient.subscribe(this.device.mqtt.topic, subscribeOptions);
  }
  publish(device, event) {
    let text = "";
    if (event.checked) {
      text = "ON";
    } else {
      text = "OFF";
    }
    this.device.payload == text;
    if (text == "ON") {
      this.color = "primary";
    } else {
      this.color = "";
    }
    let message = new App.Paho.MQTT.Message(text);
    message.destinationName = device.mqtt.topic;
    message.qos = 0;
    
    // set device payload
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': App.authToken
      };
      this.httpClient.post(App.apiUrl + 'iotSetDevicePayload',
      {
        deviceId: device.deviceId, payload: text}, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.mqttClient.send(message);
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  onSuccessCallback () {
    //console.log("==> onSuccessCallback");
  }
  onFailureCallback () {
    //console.log("==> onFailureCallback");
  }
  onConnect() {
    console.log("==> onConnect");
    //console.log(this.mqttClient.isConnected());
  }
  reconnect() {
    this.mqttClient.connect(this.options);
    if (this.mqttClient.isConnected()) {
      this.subscribe();
    }
  }
  createChart() {
    let label = "";
    let labelString = "";
    if (this.device.deviceTypeId == "DEVICE-TEMPERATURE") {
      label = "Temperature";
      labelString = "°C";
    } else if (this.device.deviceTypeId == "DEVICE-HUMIDITY") {
      label = "Humidity";
      labelString = "%";
    }
    let data = {
      labels: this.device.temperature.times,
      datasets: [
          {
              label: label,
              fill: true,
              lineTension: 10,
              backgroundColor: "rgba(75,192,192,0.4)",
              borderColor: "rgba(75,192,192,1)",
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0,
              borderJoinStyle: 'miter',
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 1,
              pointRadius: 0,
              pointHitRadius: 10,
              data: this.device.temperature.values,
              spanGaps: false,
              cubicInterpolationMode: 'monotone'
          }
        ]
    };
    let options = {
      type: 'line',
      data: data,
      options: {
        fill: false,
        responsive: true,
        scales: {
          xAxes: [{
              type: 'time',
              time: {
                displayFormats: {
                  hour: 'MMM D, hA'
                },
                distribution: 'series'
              },
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Date",
              }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
            },
            display: true,
            scaleLabel: {
              display: true,
              labelString: labelString,
            }
          }]
        }
      }
    }
    this.lineChart = new Chart(this.lineCanvas.nativeElement, options);
  }
}
