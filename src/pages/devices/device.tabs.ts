import { Component } from '@angular/core';
import { App } from '../../app/app.component';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { SchedulesDevicePage } from './device.schedules';
import { EditDevicePage } from './device.edit';
import { DeviceView } from './device.view';


@Component({
  selector: 'device-tabs',
  templateUrl: 'device.tabs.html'
})
export class DeviceTabs {
  public static device: any;
  deviceId: any;
  public static callback: any;
  tabView: any;
  tabSchedule: any;
  tabSetting: any;
  tabs: any;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public httpClient: HttpClient
    ) {
      DeviceTabs.device = this.navParams.get('device');
      DeviceTabs.callback    = this.navParams.get('callback');
      this.deviceId = this.navParams.get("deviceId");
      this.tabView = DeviceView
      this.tabSchedule = SchedulesDevicePage;
      this.tabSetting = EditDevicePage;
      console.log("create....");
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
  get device() {
    return DeviceTabs.device;
  }
}
