import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { App } from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController
  ) {
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
  get newsList() {
    return App.newsList;
  }
}
