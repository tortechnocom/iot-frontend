import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ModalController, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { NewAPIKey } from './apis.new';
import { APIKeyView } from './apis.view';

@Component({
  selector: 'apis-page',
  templateUrl: 'apis.html'
})

export class APIs {
    showPassword = false;
    apiKeyList = [];
    secretKey = "*******";
    secretColor = "";
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController
    ) {
      this.getApiKeyList();
    }
    get uiLabelMap() {
      return App.uiLabelMap;
    }
    newAPIKey() {
      var callback = () => {
        return new Promise((resolve, reject) => {
          this.getApiKeyList();
           resolve();
        });
      }
      this.nav.push(NewAPIKey, {callback: callback});
    }

    getApiKeyList() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
      this.httpClient.post(App.apiUrl + 'getApiKeyList', null, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.apiKeyList = res['apiKeyList']
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    }

    viewApiKey(apiKeyId) {
      var callback = () => {
        return new Promise((resolve, reject) => {
          this.getApiKeyList();
           resolve();
        });
      }
      this.nav.push(APIKeyView, {callback: callback, apiKeyId: apiKeyId});
    }
}