import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ModalController, ToastController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'apis-view',
  templateUrl: 'apis.view.html'
})

export class APIKeyView {
    apiKey: any;
    callback: any;
    apiSecretType= "password";
    wifiPasswordType= "password";
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController,
        public navParams: NavParams
    ) {
      this.getApiKey();
    }
    get uiLabelMap() {
      return App.uiLabelMap;
    }
    getApiKey() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
        this.httpClient.post(App.apiUrl + 'getApiKey', {apiKeyId: this.navParams.get("apiKeyId")}, {
          headers: new HttpHeaders(headerJson)
        })
        .subscribe(res => {
          let title = "Response";
          if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: res['errorMessage'],
              buttons: ['OK']
            });
            alert.present();
          } else {
            this.apiKey = res["apiKey"];
            this.callback = this.navParams.get("callback")
            this.callback();
          }
          //resolve(res);
        }, (err) => {
          console.log('err: ', err);
          //reject(err);
        });
    }
    copyToClipboard(text) {
      var textArea = document.createElement("textarea");
    
      textArea.style.position = 'fixed';
      textArea.style.top = "0";
      textArea.style.left = "0";
      textArea.style.width = '2em';
      textArea.style.height = '2em';
      textArea.style.padding = "0";
      textArea.style.border = 'none';
      textArea.style.outline = 'none';
      textArea.style.boxShadow = 'none';
      textArea.style.background = 'transparent';
      textArea.value = text;
      document.body.appendChild(textArea);
      textArea.select();
      try {
        document.execCommand('copy');
        let toast = this.toastCtrl.create({
        message: 'Copy to clipboard succesfully',
        duration: 1000
        });
        toast.present();
      } catch (err) {
        console.log('Oops, unable to copy');
      }
      document.body.removeChild(textArea);
    }
    removeApiKey() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
        this.httpClient.post(App.apiUrl + 'removeApiKey', {apiKeyId: this.apiKey.apiKeyId}, {
          headers: new HttpHeaders(headerJson)
        })
        .subscribe(res => {
          let title = "Response";
          if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: res['errorMessage'],
              buttons: ['OK']
            });
            alert.present();
          } else {
            this.callback = this.navParams.get("callback")
            this.callback();
            let toast = this.toastCtrl.create({
              message: res['successMessage'],
              duration: 1000
            });
            toast.present();
            this.nav.pop();
          }
          //resolve(res);
        }, (err) => {
          console.log('err: ', err);
          //reject(err);
        });
    }
    updateApiKey() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
        this.httpClient.post(App.apiUrl + 'iotUpdateApiKey', this.apiKey, {
          headers: new HttpHeaders(headerJson)
        })
        .subscribe(res => {
          let title = "Response";
          if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: res['errorMessage'],
              buttons: ['OK']
            });
            alert.present();
          } else {
            this.callback = this.navParams.get("callback")
            let toast = this.toastCtrl.create({
              message: res['successMessage'],
              duration: 1000
            });
            toast.present();
            this.callback();
          }
          //resolve(res);
        }, (err) => {
          console.log('err: ', err);
          //reject(err);
        });
    }
    goBack() {
      this.nav.pop();
    }
}