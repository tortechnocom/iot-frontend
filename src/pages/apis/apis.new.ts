import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ModalController, ToastController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'apis-new',
  templateUrl: 'apis.new.html'
})

export class NewAPIKey {
    input = {description: "",wifiName: "", wifiPassword: ""};
    callback: any;
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController,
        public navParams: NavParams
    ) {
      
    }
    get uiLabelMap() {
      return App.uiLabelMap;
    }
    addApiKey() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
        this.httpClient.post(App.apiUrl + 'iotGenerateApiKey', this.input, {
          headers: new HttpHeaders(headerJson)
        })
        .subscribe(res => {
          let title = "Response";
          if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: res['errorMessage'],
              buttons: ['OK']
            });
            alert.present();
          } else {
            this.callback = this.navParams.get("callback")
            this.callback();
            this.nav.pop();
          }
          //resolve(res);
        }, (err) => {
          console.log('err: ', err);
          //reject(err);
        });
    }
}