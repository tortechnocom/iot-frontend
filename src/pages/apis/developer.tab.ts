import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ModalController, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { APIs } from './apis';
import { MQTT } from './mqtt';

@Component({
  selector: 'developer-page',
  templateUrl: 'developer.tab.html'
})

export class Developer {
  tabAPIs: any;
  tabMQTT: any;
  constructor(
      public httpClient: HttpClient,
      public alertCtrl: AlertController,
      public nav: NavController,
      public modalCtrl: ModalController,
      public toastCtrl: ToastController
  ) {
    this.tabAPIs = APIs;
    this.tabMQTT = MQTT;
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
}