import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ModalController, ToastController } from 'ionic-angular';

@Component({
  selector: 'mqtt-page',
  templateUrl: 'mqtt.html'
})

export class MQTT {
    mqttPasswordType = "password";
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController
    ) {
    }
    get uiLabelMap() {
      return App.uiLabelMap;
    }
    get mqtt() {
      return App.mqtt;
    }
    copyToClipboard(text) {
      var textArea = document.createElement("textarea");
    
      textArea.style.position = 'fixed';
      textArea.style.top = "0";
      textArea.style.left = "0";
      textArea.style.width = '2em';
      textArea.style.height = '2em';
      textArea.style.padding = "0";
      textArea.style.border = 'none';
      textArea.style.outline = 'none';
      textArea.style.boxShadow = 'none';
      textArea.style.background = 'transparent';
      textArea.value = text;
      document.body.appendChild(textArea);
      textArea.select();
      try {
        document.execCommand('copy');
        let toast = this.toastCtrl.create({
        message: 'Copy to clipboard succesfully',
        duration: 1000
        });
        toast.present();
      } catch (err) {
        console.log('Oops, unable to copy');
      }
      document.body.removeChild(textArea);
    }
}