import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ViewController, NavParams } from 'ionic-angular';
import { ProfilePage } from './profile';

@Component({
  selector: 'profile-getkey',
  templateUrl: 'profile.getkey.html'
})

export class GetApiSecretKey {
    email: string;
    showPassword = false;
    apiKeyList = [];
    input = {password: ''};
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public viewCtrl: ViewController,
        public navParams: NavParams
    ) {
        
    }
    getApiSecretKey() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
      this.httpClient.post(App.apiUrl + 'getSecretKey', {
        apiKey: this.navParams.get("apiKey"), password: this.input.password
      }, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          ProfilePage.secretKey = res["secretKey"];
          ProfilePage.secretColor = "primary";
          this.viewCtrl.dismiss();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    }
    cancel() {
      this.viewCtrl.dismiss();
    }
}