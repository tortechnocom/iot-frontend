import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ModalController, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})

export class ProfilePage {
    user = {email: ''};
    showPassword = false;
    apiKeyList = [];
    public static secretKey = "*******";
    public static secretColor = "";
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController
    ) {
        this.getAccount();
    }
    getAccount() {
        let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': App.authToken
            };
          this.httpClient.post(App.apiUrl + 'getAccount', null, {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              this.user = res['user']
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
    }
    signOut() {
        return new Promise((resolve, reject) => {
          let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': App.authToken
            };
          this.httpClient.post(App.apiUrl + 'logout', null, {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              App.clearAuthToken();
              App.refreshMenu();
              this.nav.setRoot(HomePage); 
              this.nav.popToRoot();
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
        });
      }
      get uiLabelMap() {
        return App.uiLabelMap;
      }
    // generateApiKey() {
    //     var callback = () => {
    //       return new Promise((resolve, reject) => {
    //         this.getApiKeyList();
    //          resolve();
    //       });
    //     }
    //   let modal = this.modalCtrl.create(GenerateApiKey, {callback: callback});
    //   modal.present();
    // }
    // getApiSecretKey(apiKey) {
    //   let modal = this.modalCtrl.create(GetApiSecretKey, {apiKey: apiKey});
    //   modal.present();
    // }
    // hideSecretKey() {
    //   ProfilePage.secretKey = "*******";
    //   ProfilePage.secretColor = "";
    // }
    // get secretKey() {
    //   return ProfilePage.secretKey
    // }
    // get secretColor() {
    //   return ProfilePage.secretColor
    // }
    // copyToClipboard(text) {
    //   var textArea = document.createElement("textarea");
    
    //   textArea.style.position = 'fixed';
    //   textArea.style.top = "0";
    //   textArea.style.left = "0";
    //   textArea.style.width = '2em';
    //   textArea.style.height = '2em';
    //   textArea.style.padding = "0";
    //   textArea.style.border = 'none';
    //   textArea.style.outline = 'none';
    //   textArea.style.boxShadow = 'none';
    //   textArea.style.background = 'transparent';
    //   textArea.value = text;
    //   document.body.appendChild(textArea);
    //   textArea.select();
    //   try {
    //     document.execCommand('copy');
    //     let toast = this.toastCtrl.create({
    //     message: 'Copy to clipboard succesfully',
    //     duration: 1000
    //     });
    //     toast.present();
    //   } catch (err) {
    //     console.log('Oops, unable to copy');
    //   }
    //   document.body.removeChild(textArea);
    // }
}