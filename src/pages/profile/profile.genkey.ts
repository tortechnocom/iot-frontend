import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController, ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'profile-genkey',
  templateUrl: 'profile.genkey.html'
})

export class GenerateApiKey {
    email: string;
    showPassword = false;
    apiKeyList = [];
    input = {password: ''};
    callback: any;
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
        public viewCtrl: ViewController,
        public navParams: NavParams
    ) {
        
    }
    generateApiKey() {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': App.authToken
        };
      this.httpClient.post(App.apiUrl + 'generateApiKey', this.input, {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.callback = this.navParams.get("callback");
          this.callback();
          this.viewCtrl.dismiss();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    }
    cancel() {
      this.viewCtrl.dismiss();
    }
}